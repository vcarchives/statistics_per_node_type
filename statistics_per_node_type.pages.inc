<?php
// $Id: statistics_per_node_type.pages.inc 347 2010-12-29 09:10:07Z andy63 $

/**
 * @file Admin pages -- settings & report form.
 */

/**
 * Settings form.
 *
 * @TODO: Button to flush/restart the stats.
 */
function statistics_per_node_type_settings() {
  $form['statistics_per_node_type_select'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Default options'),
    '#default_value' => variable_get('statistics_per_node_type_select', array()),
    '#options' => _statistics_per_node_type_node_types_options(),
    '#description' => t('Users with the <em>administer nodes</em> permission will be able to override these options.'),
  );

  return system_settings_form($form);
}

/**
 * Helper function to render the node-type-options.
 */
function _statistics_per_node_type_node_types_options() {
  $options = array();
  foreach(node_get_types() as $type) {
    $options[$type->type] = $type->name;
  }

  return $options;
}

/**
 * Per Node Type Stats page.
 */
function statistics_per_node_type_graph() {
  $types = variable_get('statistics_per_node_type_select', array());
  $types = array_filter($types);

  if (count($types) == 0) {
    drupal_set_message(
      t(
        'Not selected node types to display statistics in !link', array(
          '!link' => l(
            'Statistics per node type settings',
            'admin/settings/statistics_per_node_type'
          ),
        )
      ),
      'error'
    );

    return '';
  }

  $days = array(90, 60, 30, 7);
  
  $output = '';

  // Get stats-data and render it
  foreach (statistics_per_node_type_trafic($types, $days) as $day => $stats) {
    $labels = $values = array();
    foreach ($stats as $node_type => $counter) {
      $labels[] = ucwords(str_replace('_', '+', $node_type)) . " ({$counter})";
      $values[] = $counter;
    }

    $output .= sprintf(
      '<div style="float: left; padding-right: 15px;">%s</div>',
      theme('fieldset', array(
        '#title' => format_interval(time() - strtotime("- {$day} days"), 1),
        '#value' => sprintf(
          '<div style="padding: 15px 0 0;"><img src="'
            . 'http://chart.apis.google.com/chart?cht=p3&chs=%s'
              . '&chdl=%s'
              . '&chd=t:%s'
              . '&chma=|25'
              . '&chdlp=l'
              . ''
            . '" /></div>',
          STATISTICS_PER_NODE_TYPE_SIZE,
          implode('|', $labels),
          implode(',', $values)
        ),
      ))
    );
  }

  return $output;
}

/**
 * Get stats data by node-types and days.
 */
function statistics_per_node_type_trafic($node_types, $days) {
  $stats = array();

  // Format node types
  foreach (array_keys($node_types) as $k) {
    $node_types[$k] = "'{$node_types[$k]}'";
  }

  $node_types = implode(', ', $node_types);

  foreach ($days as $day) {
    $sql  = <<<SQL
      SELECT COUNT(*) AS counter, n.`type`
      FROM {statistics_per_node_type} s
        INNER JOIN {node} n ON s.nid = n.nid
      WHERE s.`timestamp` >= (24 * 3600 * %d) AND n.`type` IN ({$node_types})
      GROUP BY n.`type`
SQL;

    $query = db_query($sql, $day);
    while ($v = db_fetch_object($query)) {
      $stats[$day][$v->type] = $v->counter;
    }
  }

  return $stats;
}
